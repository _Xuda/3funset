package funsets

object Main extends App {
  import FunSets._
  println(contains(singletonSet(1), 1))

  printSet(union(union(singletonSet(1), singletonSet(3)) , union(singletonSet(1), singletonSet(2))))

  println(forall(union(singletonSet(1), singletonSet(2)), (a: Int) => a<5))

  val s = union(union(union(singletonSet(1), singletonSet(2)),union(singletonSet(3),singletonSet(4))),union(union(singletonSet(5), singletonSet(7)),singletonSet(1000)))
  printSet(s)

  val res1 = map(s, (x)=>x-1)
  printSet(res1)

}
